const twilio = require('twilio');
const privateinfo = require('./privateInfo');
const https = require('https');

console.log(privateinfo.twilioSID);
console.log(privateinfo.twilioKey);
console.log(privateinfo.myTwilioNumber);
console.log(privateinfo.myPhoneNumber);

const client = new twilio(privateinfo.twilioSID, privateinfo.twilioKey);

var DarkSkyAPICall = `https://api.darksky.net/forecast/${privateinfo.darkSkyAPIKey}/${privateinfo.myLong},${privateinfo.myLat}`;
console.log(DarkSkyAPICall);

var currentTempF = 'This is broken';

https.get(DarkSkyAPICall, (resp) => {
    let data = '';

    resp.on('data', (chunk) => {
        data += chunk;
    });

    resp.on('end', () => {
        currentTempF = JSON.parse(data).currently.temperature;
        console.log(JSON.parse(data).currently.temperature);

        client.messages.create({
            to: privateinfo.myPhoneNumber,
            from: privateinfo.myTwilioNumber,
            body: `The weather right now is ${currentTempF} degrees F.`
        });
    });

}).on("error", (err) => {
    console.log("Error: " + err.message);
});
